import data

def enroll(course, student):
    
   if student in data.students and course in data.courses:
       data.enrollments.append((course,student))
       return True

   return False